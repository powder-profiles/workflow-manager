#!/bin/sh
set -x

password="{{ grafana_admin_password }}"
tmpdir="{{ grafana_setup_dir }}"

cd $tmpdir

#
# Wait for influx to be ready.
#
while true;
do
    influx ping
    if [ $? -eq 0 ]; then
	break
    fi
    echo "Waiting for influx to ping ..."
    sleep 5
done
sleep 15

#
# This is just for the web interface
#
influx org find -n metrics
if [ $? -ne 0 ]; then
    influx setup --username metrics --password $password -org metrics --bucket metrics --force
    if [ $? -ne 0 ]; then
	echo 'ERROR: failed to create metrics bucket user'
	exit 1
    fi
fi

#
# To create a DB password we need the bucket id (why not the name?).
#
bucketid=`influx bucket ls | grep metrics | awk '{print $1}'`
if [ $? -ne 0 -o -z "$bucketid" ]; then
    echo "ERROR: could not get the bucket id."
    exit 1
fi

#
# This user is for DB manipulation.
#
influx v1 auth find -o metrics --username metrics
if [ $? -ne 0 ]; then
    influx v1 auth create -o metrics --username metrics --password $password \
	   --write-bucket $bucketid --read-bucket $bucketid
    if [ $? -ne 0 ]; then
	echo 'ERROR: failed to setup influxdb database user'
	exit 1
    fi
fi

exit 0
