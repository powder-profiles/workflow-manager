#!/bin/sh
set -x

password="{{ grafana_admin_password }}"
tmpdir="{{ grafana_setup_dir }}"
port="{{ grafana_localhost_port }}"

cd $tmpdir

#
# Wait for grafana to be ready.
#
while true;
do
    curl -s http://localhost:$port
    if [ $? -eq 0 ]; then
	break
    fi
    echo "Waiting for grafana to ping ..."
    sleep 5
done

grafana cli --homepath /usr/share/grafana admin reset-admin-password $password
if [ $? -ne 0 ]; then
    echo 'ERROR: setting grafana password with grafana-cli failed'
    exit 1
fi

#
# Add the influx datasource
#
goo="{\"name\": \"InfluxDB2\",\"type\": \"influxdb\",\"uid\": \"OzcR1Jo4j\",\"url\": \"http://influxdb:8086\",\"secureJsonFields\": {\"password\": true},\"user\": \"metrics\",\"database\": \"metrics\",\"isDefault\": true,\"editable\": true,\"basicAuth\":false,\"basicAuthUser\":\"\",\"basicAuthPassword\":\"\",\"withCredentials\": false,\"access\":\"proxy\",\"secureJsonData\":{\"password\": \"${password}\"}}"

curl -u admin:$password --header 'Accept: application/json' \
     --header 'Content-Type: application/json' \
     -d "$goo" http://localhost:$port/api/datasources
if [ $? -ne 0 ]; then
    echo 'ERROR: adding influxdb datasource to grafana failed'
    exit 1
fi

#
# The dashboard if we got one.
#
dashboard="dashboard.json"
if [ -e $dashboard ]; then
    curl --header 'Accept: application/json' --header 'Content-Type: application/json' -u admin:$password -d @${dashboard} http://localhost:$port/api/dashboards/db
    if [ $? -ne 0 ]; then
	echo 'ERROR: adding metrics dashboard to grafana failed'
	exit 1
    fi
fi

exit 0
