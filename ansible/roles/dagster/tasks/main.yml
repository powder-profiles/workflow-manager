# install dagster/dagit via docker, then docker-compose, and a reverse proxy

# XXX This sucks; we need emulab.common.common to split out the per-expt
# ssh stuff so we can leverage it.
- name: Get public half of Emulab per-expt key
  when: geni_pubkey is not defined
  shell: "ssh-keygen -f /users/{{ emulab_swapper }}/.ssh/id_rsa_perexpt -y"
  register: ret

- ansible.builtin.set_fact:
    geni_pubkey: "{{ ret.stdout_lines[0] }}"
  when: geni_pubkey is not defined

- name: Create Dagster setup dir
  become_user: "{{ emulab_swapper }}"
  become: true
  ansible.builtin.file:
    path: "{{ dagster_setup_dir }}"
    state: directory

- name: Create Dagster application dir
  become_user: "{{ emulab_swapper }}"
  become: true
  ansible.builtin.file:
    path: "{{ dagster_setup_dir }}/app"
    state: directory
    mode: '0777'

- name: Stub definition file
  when: "dagster_workflow_repo == ''"
  become_user: "{{ emulab_swapper }}"
  become: true
  ansible.builtin.copy:
    src: templates/dagster-project/
    dest: "{{ dagster_setup_dir }}/app"

- name: Clone the workflow definitions repo if defined
  when: "dagster_workflow_repo != ''"
  block:
    - name: Cloning workflow repo
      become_user: "{{ emulab_swapper }}"
      become: true
      shell: "git clone {{ dagster_workflow_repo }} {{ dagster_setup_dir }}/workflow/"
      register: ret
      until: ret.rc == 0
      retries: 30
      delay: 5

    - name: Set workflow repo permissions so nginx can update it.
      become_user: "{{ emulab_swapper }}"
      become: true
      shell: "chmod -R og+w {{ dagster_setup_dir }}/workflow"

    - name: Set workflow repo to shared so nginx can update it.
      become_user: "{{ emulab_swapper }}"
      become: true
      shell: "git init --shared=all {{ dagster_setup_dir }}/workflow"

    - name: Checking if pipeline code exists
      stat:
        path: "{{ dagster_setup_dir }}/workflow/dagster/dagster_project"
      register: pipeline_data
  
    - name: Copy pipeline code into place
      become_user: "{{ emulab_swapper }}"
      become: true
      shell: "rsync -av {{ dagster_setup_dir }}/workflow/dagster/ {{ dagster_setup_dir }}/app"
      when: pipeline_data.stat.exists

    - name: Copy stub definition.py instead
      become_user: "{{ emulab_swapper }}"
      become: true
      ansible.builtin.copy:
        src: templates/dagster-project/
        dest: "{{ dagster_setup_dir }}/app"
      when: not pipeline_data.stat.exists

- name: Add python docker packages
  ansible.builtin.package:
    name:
      - python{{ ansible_python_version | first | replace('2','') }}-docker
      - docker-compose
    state: present

- name: Create docker-compose.yml
  become_user: "{{ emulab_swapper }}"
  become: true
  ansible.builtin.template:
    src: docker-compose.yml
    dest: "{{ dagster_setup_dir }}/docker-compose.yml"

- name: Create dagster.yaml
  become_user: "{{ emulab_swapper }}"
  become: true
  ansible.builtin.template:
    src: dagster.yaml
    dest: "{{ dagster_setup_dir }}/dagster.yaml"

- name: Create workspace.yaml
  become_user: "{{ emulab_swapper }}"
  become: true
  ansible.builtin.template:
    src: workspace.yaml
    dest: "{{ dagster_setup_dir }}/workspace.yaml"

- name: Create dockerfile-dagster
  become_user: "{{ emulab_swapper }}"
  become: true
  ansible.builtin.template:
    src: dockerfile-dagster
    dest: "{{ dagster_setup_dir }}/dockerfile-dagster"

- name: Create dockerfile-usercode
  become_user: "{{ emulab_swapper }}"
  become: true
  ansible.builtin.template:
    src: dockerfile-usercode
    dest: "{{ dagster_setup_dir }}/dockerfile-usercode"

- name: Create dockerfile-unified
  become_user: "{{ emulab_swapper }}"
  become: true
  ansible.builtin.template:
    src: dockerfile-unified
    dest: "{{ dagster_setup_dir }}/dockerfile-unified"

- name: Create creds.py
  become_user: "{{ emulab_swapper }}"
  become: true
  ansible.builtin.template:
    src: creds.py
    dest: "{{ dagster_setup_dir }}/app/dagster_project/workflowCreds.py"

- name: Create workflowDefs.py.py
  become_user: "{{ emulab_swapper }}"
  become: true
  ansible.builtin.template:
    src: workflowDefs.py
    dest: "{{ dagster_setup_dir }}/app/dagster_project/workflowDefs.py"

- name: Install python passlib package for htpasswd
  when: ansible_env["VIRTUAL_ENV"] == ""
  ansible.builtin.package:
    name: python{{ ansible_python_version | first | replace('2','') }}-passlib

- name: Install python passlib in venv for htpasswd
  when: ansible_env["VIRTUAL_ENV"] != ""
  ansible.builtin.pip:
    name: passlib

- name: Create Dagit password
  ansible.builtin.htpasswd:
    name: admin
    password: "{{ dagster_admin_password }}"
    path: "{{ dagster_setup_dir }}/htpasswd"
    state: present

- name: Create backend dagster reload script.
  ansible.builtin.template:
    src: dagster-reload.py
    dest: "{{ dagster_setup_dir }}/dagster-reload.py"
    mode: '0755'

- name: Start up Dagster via docker-compose
  community.docker.docker_compose_v2:
    project_src: "{{ dagster_setup_dir }}"
    state: present
  register: ret
  until: ret is success
  retries: 30
  delay: 5

- name: Wait until Dagster is up
  shell: docker compose exec -T docker_dagster_unified whoami
  args:
    chdir: "{{ dagster_setup_dir }}"
  register: ret
  until: ret.rc == 0
  retries: 120
  delay: 5

- name: Create nginx reverse proxy for dagster
  when: dagster_reverse_proxy_key_path and dagster_reverse_proxy_cert_path
  block:
    - name: Install nginx
      ansible.builtin.package:
        name: nginx
        state: present

    - name: Install fcgiwrap
      ansible.builtin.package:
        name: fcgiwrap
        state: present

    - name: Create fcgiwrap config file
      ansible.builtin.template:
        src: nginx-fcgiwrap.conf
        dest: /etc/nginx/fcgiwrap.conf
        mode: '0755'

    - name: Create /var/www/cgi-bin
      ansible.builtin.file:
        path: /var/www/cgi-bin
        state: directory
        mode: '0755'

    - name: Create nginx cgi bin reload script
      ansible.builtin.template:
        src: dagster-reload.pl
        dest: /var/www/cgi-bin/dagster-reload
        mode: '0755'

    - name: Oh this is bad.
      shell: "git config -f /var/www/.gitconfig --add safe.directory {{ dagster_setup_dir }}/workflow"

    - name: Allow the web server to run docker-compose. Ick, I know. 
      ansible.builtin.template:
        src: sudoers-nginx
        dest: /etc/sudoers.d/99-dockerwww
        mode: '0640'

    - name: Create reverse proxy site
      ansible.builtin.template:
        src: nginx-reverse-proxy.conf
        dest: /etc/nginx/sites-available/dagster-reverse-proxy.conf
        mode: '0755'
      notify:
        - Reload nginx

    - name: Enable reverse proxy site
      ansible.builtin.file:
        src: /etc/nginx/sites-available/dagster-reverse-proxy.conf
        dest: /etc/nginx/sites-enabled/dagster-reverse-proxy.conf
        state: link
      notify:
        - Reload nginx

