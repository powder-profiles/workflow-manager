
EMULAB_UID        = "{{ emulab_swapper }}"
EMULAB_PID        = "{{ emulab_pid }}"
# Defined in the grafana ansible role. Not sure what to do about that.
GRAFANA_HOSTPORT  = "{{ emulab_fqdn }}:8446"
INFLUXDB_HOSTPORT = "{{ emulab_fqdn }}:8447"
# We use the same password across all roles. Same problem as above.
GRAFANA_PASSWORD  = "{{ grafana_admin_password }}"
INFLUXDB_PASSWORD = "{{ grafana_admin_password }}"
