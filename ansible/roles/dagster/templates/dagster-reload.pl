#!/bin/perl

my $WORKDIR = "{{ dagster_setup_dir }}";
my $REPODIR = "$WORKDIR/workflow";

sub ExecQuiet($);
sub Finished($$);

# We are running as the web server, make sure new files get created og+w
umask(0000);

print "Content-Type: text/plain\n";
print "Access-Control-Allow-Origin: *\n\n";

#
# pull the repo over.
#
my $output = ExecQuiet("cd $REPODIR; git pull");
if ($?) {
    Finished(1, $output);
}

#
# Need the docker container 
#
my $container = `sudo /bin/docker ps | grep dagster | awk '{print \$1}'`;
if ($?) {
    Finished(1, "Could not get docker container ID");
}
chomp($container);

#
# Just the dagster part is copied to the container app directory
#
$output = ExecQuiet("sudo /bin/docker cp $REPODIR/dagster/dagster_project ".
		    "   ${container}:${WORKDIR}/app");
if ($?) {
    Finished(1, $output);
}

#
# And this little ditty is a python fragment that talks to dagster
# and tells it to reload the repository. 
#
$output = ExecQuiet("cd $WORKDIR; sudo /bin/docker-compose exec -T ".
		    "  docker_dagster_unified $WORKDIR/home/dagster-reload.py");
if ($?) {
    Finished(1, $output);
}

Finished(0, "Your dagster definitions have been successfully reloaded");

#
# We want to return output if we fail.
#
sub Finished($$)
{
    my ($code, $output) = @_;
    print $output . "\n";
    exit($code);
}

#
# Run a command, being sure to capture all output. 
#
sub ExecQuiet($)
{
    my ($command) = @_;
    my $output    = "";
    
    #
    # This open implicitly forks a child, which goes on to execute the
    # command. The parent is going to sit in this loop and capture the
    # output of the child. We do this so that we have better control
    # over the descriptors.
    #
    my $pid = open(PIPE, "-|");
    if (!defined($pid)) {
	print STDERR "ExecQuiet Failure; popen failed!\n";
	return -1;
    }
    
    if ($pid) {
	while (<PIPE>) {
	    $output .= $_;
	}
	close(PIPE);
    }
    else {
	open(STDERR, ">&STDOUT");
	exec($command);
    }
    return $output;
}

