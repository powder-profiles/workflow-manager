FROM python:3.10-slim

# Checkout and install dagster libraries needed to run the gRPC server
# exposing your repository to dagit and dagster-daemon, and to load the DagsterInstance

RUN apt-get -y update
RUN apt-get -y install git

RUN pip install \
    dagster \
    dagster-postgres \
    dagster-docker \
    dagster-shell \
    robotframework \
    robotframework-sshlibrary \
    tcms-api \
    future \
    lxml

RUN pip install git+https://gitlab.flux.utah.edu/stoller/portal-tools.git

# Repository code is mounted here
WORKDIR {{ dagster_setup_dir }}/app

EXPOSE 4000

# CMD allows this to be overridden from run launchers or executors that want
# to run other commands against your repository
CMD ["dagster", "api", "grpc", "-h", "0.0.0.0", "-p", "4000", "-f", "definition.py"]
