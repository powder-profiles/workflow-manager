from __future__ import absolute_import

#
# Stub code for starting up Dagster first time, intended to be replaced later.
#
from dagster import fs_io_manager, job, op, Definitions

resource_defs = {
    "io_manager": fs_io_manager.configured({"base_dir":
                                            "/tmp/io_manager_storage"})
}

@op
def hello():
    return 1


@op
def goodbye(foo):
    if foo != 1:
        raise Exception("Bad io manager")
    return foo * 2

@job(
    resource_defs = resource_defs
)
def myjob():
    goodbye(hello())
    pass

defs = Definitions(
    jobs=[myjob],
    resources=resource_defs
)
